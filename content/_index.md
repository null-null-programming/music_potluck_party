---
title: "Music Potluck Party"

description: "~ VRChat 音楽投げ会 ~"
cascade:
  featured_image: 'https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/static/images/VRChat_Yokohama.png'
---

Music Potluck PartyはVRchat上のQuest対応イベントです
参加者のみなさまが最近良かった曲やずっと好きだった曲を持ち寄り、みんなで聴く音楽イベントです。音楽を聞くだけでも大丈夫なのでお気軽にご参加ください！   

参加をする際は事前にこのサイトのRuleページに目を通してください！