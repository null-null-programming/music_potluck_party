---
date: 2023-2-10
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/57191394f402f8574792252af8b5e5368c7c0685/content/post/8/VRChat_2023-02-10_21-03-19.830_1920x1080.png"
tags: []
title: "第8回 Music Potluck Party"
---
第8回音楽投げ会を開催しました！

## 今回の参加者

- 狐ノ巳 (konomi)
- xxseekxx
- つもふか
- KuRo_IrO
- 出汁の粉
- SpinelSC
- Takahashiwelt
- Grace____aq
- なな/N4NA
- かげひと

## 今回流れた音楽 

- KuRo_IrO : ND Lee - 아침형 인간 BGA
[![](https://img.youtube.com/vi/0kW8hcseiZ8/0.jpg)](https://www.youtube.com/watch?v=0kW8hcseiZ8)

- セロトニン - U1 overground
[![](https://img.youtube.com/vi/Kq8UAOpKGjQ/0.jpg)](https://www.youtube.com/watch?v=Kq8UAOpKGjQ)

- M2U X Pica - Wings of Silence
[![](https://img.youtube.com/vi/1uNnu7c_Df0/0.jpg)](https://www.youtube.com/watch?v=1uNnu7c_Df0)

- Fall Out Boy - Dance, Dance
[![](https://img.youtube.com/vi/C6MOKXm8x50/0.jpg)](https://www.youtube.com/watch?v=C6MOKXm8x50)

- Layo & Bushwacka - Lovestory
[![](https://img.youtube.com/vi/tD3y8RoTjSY/0.jpg)](https://www.youtube.com/watch?v=tD3y8RoTjSY)

- Fever The Ghost - SOURCE (official music video)
[![](https://img.youtube.com/vi/9RHFFeQ2tu4/0.jpg)](https://www.youtube.com/watch?v=9RHFFeQ2tu4)

- ラグトレイン - 稲葉曇 / Covered by 朝日南アカネ
[![](https://img.youtube.com/vi/f1eouLsfZ-0/0.jpg)](https://www.youtube.com/watch?v=f1eouLsfZ-0)

- The 1975 - Chocolate (Official Video)
[![](https://img.youtube.com/vi/CHk5SWVO4p8/0.jpg)](https://www.youtube.com/watch?v=CHk5SWVO4p8)

- MIGHTY OBSTACLE
[![](https://img.youtube.com/vi/BV-S6TpIBQs/0.jpg)](https://www.youtube.com/watch?v=BV-S6TpIBQs)

- yama 『麻痺』MV
[![](https://img.youtube.com/vi/2By3feOVw20/0.jpg)](https://www.youtube.com/watch?v=2By3feOVw20)

- Nightmare - M2U
[![](https://img.youtube.com/vi/Nsg2CPMeb7c/0.jpg)](https://www.youtube.com/watch?v=Nsg2CPMeb7c)

- !!! (Chk, Chk, Chk) - Must Be The Moon
[![](https://img.youtube.com/vi/abH7T8j0ITU/0.jpg)](https://www.youtube.com/watch?v=abH7T8j0ITU)

- run
[![](https://img.youtube.com/vi/P_vhYdFCnSU/0.jpg)](https://www.youtube.com/watch?v=P_vhYdFCnSU)

- Sugar&Co - Sea Saw Days (Kirara Magic Remix)
[![](https://img.youtube.com/vi/2xyYhJCW3tI/0.jpg)](https://www.youtube.com/watch?v=2xyYhJCW3tI)

- Circus Maximus
[![](https://img.youtube.com/vi/YiaRlmSIyao/0.jpg)](https://www.youtube.com/watch?v=YiaRlmSIyao)


## 感想・反省点   
今回は10名の方が来てくれました！   
記事が遅れてしまい申し訳ないです～!!!   
いつもありがとうございます！　　　