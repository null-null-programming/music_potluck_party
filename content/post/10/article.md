---
date: 2023-3-3
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/content/post/10/VRChat_2023-03-03_21-03-09.601_1920x1080.png"
tags: []
title: "第10回 Music Potluck Party"
---
第10回音楽投げ会を開催しました！

## 今回の参加者

- Cipher-pCode-9さん
- かのせしずくさん
- つもふかさん
- いるまさん
- T4LLYさん
- UraHakuShuさん
- 狐ノ巳さん
- pe_chanさん
- 出汁の粉さん

## 今回流れた音楽 

- Cosmograph - DATAERR0R
[![](https://img.youtube.com/vi/mevx85e9QH8/0.jpg)](https://www.youtube.com/watch?v=mevx85e9QH8)

- Heartbeat - Friday Night Funkin': Vs. Impostor V4
[![](https://img.youtube.com/vi/YzV34YpWixs/0.jpg)](https://www.youtube.com/watch?v=YzV34YpWixs)

- ピノキオピー - 匿名M feat. 初音ミク・ARuFa / Anonymous M
[![](https://img.youtube.com/vi/yiqEEL7ac6M/0.jpg)](https://www.youtube.com/watch?v=yiqEEL7ac6M)

- David Guetta & MORTEN - Make It To Heaven (with Raye) (Lyric video)

[![](https://img.youtube.com/vi/SahRw77yvx0/0.jpg)](https://www.youtube.com/watch?v=SahRw77yvx0)

- Vivid BAD SQUAD × 巡音ルカ
[![](https://img.youtube.com/vi/X_u5UtcgrKw/0.jpg)](https://www.youtube.com/watch?v=X_u5UtcgrKw)

- Mobile Suit Gundam Thunderbolt OST - 09. あなたのお相手 ~I'm your baby

[![](https://img.youtube.com/vi/o_JS_4AE2Vc/0.jpg)](https://www.youtube.com/watch?v=o_JS_4AE2Vc)

- Eurythmics, Annie Lennox, Dave Stewart - Sweet Dreams (Are Made Of This) (Official Video)
[![](https://img.youtube.com/vi/bG9z-atG7gc/0.jpg)](https://www.youtube.com/watch?v=bG9z-atG7gc)

- Skrillex - Scary Monsters And Nice Sprites (Official Audio)
[![](https://img.youtube.com/vi/WSeNSzJ2-Jw/0.jpg)](https://www.youtube.com/watch?v=WSeNSzJ2-Jw)

- Lyfer - Out of Place (Friday night: Monsters of Monsters)
[![](https://img.youtube.com/vi/tmPGU0BrXfk/0.jpg)](https://www.youtube.com/watch?v=tmPGU0BrXfk)

- 【EZ2ON REBOOT : R】 JUSTITIA / Cosmograph vs. Lunatic Sounds (feat. NieN)
[![](https://img.youtube.com/vi/3cO3h9eGmQk/0.jpg)](https://www.youtube.com/watch?v=3cO3h9eGmQk)

- 【俺ステ】Our only answer
[![](https://img.youtube.com/vi/UE0ZcuBg1YM/0.jpg)](https://www.youtube.com/watch?v=UE0ZcuBg1YM)

- MIO Men of destiny Guitar cover 弾いてみた gundam 0083 stardust memory 機動戦士ガンダム ガトー少佐大好き
[![](https://img.youtube.com/vi/w_S_Rh3GkQY/0.jpg)](https://www.youtube.com/watch?v=w_S_Rh3GkQY)

- Nine Point Eight (Lyric Video)
[![](https://img.youtube.com/vi/Le5nXTvNYJc/0.jpg)](https://www.youtube.com/watch?v=Le5nXTvNYJc)

- 2You - Nicolas Duque
[![](https://img.youtube.com/vi/-RuniXu__yM/0.jpg)](https://www.youtube.com/watch?v=-RuniXu__yM)

- 【和訳】This is me (lyrics) from 'The Greatest Showman' グレイテストショーマン
[![](https://img.youtube.com/vi/7YcH5f1LcJI/0.jpg)](https://www.youtube.com/watch?v=7YcH5f1LcJI)

- どんな結末がお望みだい？ / ワンダーランズ×ショウタイム × 初音ミク

[![](https://img.youtube.com/vi/1W-7h8LO-Hg/0.jpg)](https://www.youtube.com/watch?v=1W-7h8LO-Hg)

- HASAMI group - 新元号は平成 / PIANO
[![](https://img.youtube.com/vi/WWHKnrdNU58/0.jpg)](https://www.youtube.com/watch?v=WWHKnrdNU58)

- Knuckle G - The Midnight Love
[![](https://img.youtube.com/vi/zxLjfdGQ_FI/0.jpg)](https://www.youtube.com/watch?v=zxLjfdGQ_FI)

## 感想・反省点   
今回は9名の方が来てくれました！   
いつもありがとうございます！　　　