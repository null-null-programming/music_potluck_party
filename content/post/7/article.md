---
date: 2023-2-3
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/content/post/7/VRChat_2023-02-03_21-01-47.162_1920x1080.png"
tags: []
title: "第7回 Music Potluck Party"
---
第7回音楽投げ会を開催しました！

## 今回の参加者

- Sarai Takemotoさん   
- Saori_xxiさん   
- かげひとさん   
- SpinelSCさん   
- いるまさん   
- Cipher-pCode-9さん   
- ジェヨンさん   
- 狐ノ巳さん   
- きるけーさん
- ブルーワームさん   
- furomendoさん   

以上11名の方が遊びに来てくれました！   
ありがとうございます～

## 今回流れた音楽 

- What You Afraid Of (Disco Phobia Mix)　　　
[![](https://img.youtube.com/vi/DtxPPP1Tt50/0.jpg)](https://www.youtube.com/watch?v=DtxPPP1Tt50)

私の選曲です。   
先週行われたリアルイベント「電気ショック」でも流した曲なのですが、ボーカルがめちゃくちゃ好きで気に入っています。　　　

- DR BOMBAY "Sos (The Tiger Took My Family)
[![](https://img.youtube.com/vi/kPpuEgAZ2rQ/0.jpg)](https://www.youtube.com/watch?v=kPpuEgAZ2rQ)

かげひとさんの選曲です。   
不思議な曲ですね・・・　　　

- Cö shu Nie – 絶体絶命 (Official Video)　/ “約束のネバーランド”ED　　　
[![](https://img.youtube.com/vi/6WNk9yMNovs/0.jpg)](https://www.youtube.com/watch?v=6WNk9yMNovs)

irumaさんの選曲です。   
ボーカルの歌い方が特に好きですね～   

- EmoCosine vs. RiraN - A Showdown Between Two Prideful Geniuses (Extended Mix)
[![](https://img.youtube.com/vi/d3Hd2d1b3ks/0.jpg)](https://www.youtube.com/watch?v=d3Hd2d1b3ks)

SpinelSCさんの選曲です。   
さわやかな部分と重めの部分がありますが、私はさわやかな部分が好きです。   

- HASAMI group - IQ500の蕎麦屋
[![](https://img.youtube.com/vi/yftOFLKgPFA/0.jpg)](https://www.youtube.com/watch?v=yftOFLKgPFA)

Saori_xxiさんの選曲です。   
00:59からの疾走感好きです。 　　　

- aivi & surasshu - Mabe Village (Piano/Chiptune Cover)
[![](https://img.youtube.com/vi/FLXOQA6fJC8/0.jpg)](https://www.youtube.com/watch?v=FLXOQA6fJC8)

Sarai_Takemotoさんの選曲です。　　　
チップチューンとピアノのハーモニーが良いですね～　　　

- Reflec Beat VOLZZA - HASTUR (Extended Mix)
[![](https://img.youtube.com/vi/4Sj3l7FCL78/0.jpg)](https://www.youtube.com/watch?v=4Sj3l7FCL78)

狐ノ巳さんの選曲です。　　　
00:56からのドロップが好きです。   

- DUSTCELL - LAZY
[![](https://img.youtube.com/vi/F6KgJox-NmM/0.jpg)](https://www.youtube.com/watch?v=F6KgJox-NmM)

irumaさんの選曲です。   
ラップ部分と歌パートがあるのが良いですね。   

- 【BOF:ET】😲🙌👾🤖🤸👺🌠🤲👻🖐️🛸😀
[![](https://img.youtube.com/vi/TM93ntGYLGg/0.jpg)](https://www.youtube.com/watch?v=TM93ntGYLGg)

SpinelSCさんの選曲です。   
動画も音楽もかわいい。   

- 笹川真生 - うろんなひと / Mao Sasagawa - Suspicious Person
[![](https://img.youtube.com/vi/dNMPBIWp6H4/0.jpg)](https://www.youtube.com/watch?v=dNMPBIWp6H4)

Saori_xxiさんの選曲です。   
音楽も良いのですが動画の雰囲気がとても良いですね。   

- 路上のキリジン     
[![](https://img.youtube.com/vi/LYhuBOOBunU/0.jpg)](https://www.youtube.com/watch?v=LYhuBOOBunU)

かげひとさんの選曲です。   
全体的に古い雰囲気があってとても良いですね。   

- Zedd & Jasmine Thompson - Funny (Lyric Video)   
[![](https://img.youtube.com/vi/JzpxJcJwDZ0/0.jpg)](https://www.youtube.com/watch?v=JzpxJcJwDZ0)

Cipher-pCode-9さんの選曲です。   
EDMって感じで良いですね。   

- flower『トラフィック・ジャム』
[![](https://img.youtube.com/vi/oUevY6uH4Qg/0.jpg)](https://www.youtube.com/watch?v=oUevY6uH4Qg)

ジェヨンさんの選曲です。   
ボカロ良いっすね。   

- Raiden V OST- Unknown Pollution   
[![](https://img.youtube.com/vi/PX3vEQ-KQvs/0.jpg)](https://www.youtube.com/watch?v=PX3vEQ-KQvs)

狐ノ巳さんの選曲です。   
シューティングゲームの１面の曲らしいです。   
めっちゃ爽快で良い曲ですね～   
ギターがとても良い～　　　

- くるり - 琥珀色の街、上海蟹の朝
[![](https://img.youtube.com/vi/NyddMMiViZc/0.jpg)](https://www.youtube.com/watch?v=NyddMMiViZc)

ブルーワームさんの選曲です。   
この曲めっちゃ良いですよね~　　　
メロディがとても好きです。    

- AFRO CELT SOUND SYSTEM - Release Masters at Work Main Mix
[![](https://img.youtube.com/vi/_YxeWeZE26Y/0.jpg)](https://www.youtube.com/watch?v=_YxeWeZE26Y)

私の選曲です。   
2:54からのバグパイプみたいな音がとても好きですね～   

## 感想・反省点   
今回は11名の方が来てくれました！   
記事が遅れてしまい申し訳ないです～!!!   
いつもありがとうございます！　　　