---
date: 2023-1-24
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/content/post/6/VRChat_2023-01-24_21-00-52.676_1920x1080.png"
tags: []
title: "第6回 Music Potluck Party"
---
第6回音楽投げ会を開催しました！

## 今回の参加者

- kakoonさん
- はなちゃんさん
- つもふかさん
- Takahashiweltさん
- SpinelSCさん
- 狐ノ巳 (konomi)さん
- つんつくつん！さん
- Nanachidesuさん
- Lunamia_さん
- Sirulu 56f0さん

以上10名の方が遊びに来てくれました！   
ありがとうございます～

## 今回流れた音楽 

- 渚のアデリーヌ
[![](https://img.youtube.com/vi/GY_ac7D36TY/0.jpg)](https://www.youtube.com/watch?v=GY_ac7D36TY)

はなちゃんさんの選曲です。   
はなちゃんさんはよくピアノの古い曲をかけてくださるのですが、これもとても良いですね。   

- Street - Martitime Police
[![](https://img.youtube.com/vi/fkRt1YFNzes/0.jpg)](https://www.youtube.com/watch?v=fkRt1YFNzes)

私の選曲です。   
私が京都に住んでいた時の夏に聴いていた曲です。   
これを聴くと蒸し暑い夏を思い出してエモくなります。   

- METAFIVE - Don’t Move -Studio Live Version-
[![](https://img.youtube.com/vi/7LBUEYGfisQ/0.jpg)](https://www.youtube.com/watch?v=7LBUEYGfisQ)

Takahashiweltさんの選曲です。   
楽器の生音にキャッチーなボーカルがとても好きです。   
weltさんの選曲毎回好みなんですよね～～～   

- passionate fate (Extended RRVer.) - Ryu☆ 
[![](https://img.youtube.com/vi/CPfzGk17o58/0.jpg)](https://www.youtube.com/watch?v=CPfzGk17o58)

狐ノ巳 (konomi)さんの選曲です。   
重いキックにシンセがのっていて良いですね。   
全然関係ないんですけど狐ノ巳さんの鏡文字がとてもすごいんですよね～   
VRCで鏡文字練習会を開かれているそうなので興味のある方は行かれてみては？　　　

- snooze / wotaku feat. 初音ミク(Hatsune Miku)
[![](https://img.youtube.com/vi/fqBpGiVn2k0/0.jpg)](https://www.youtube.com/watch?v=fqBpGiVn2k0)

kakoon/カクーンさんの選曲です。   
小気味良いリズム感のある曲で好きですね～   
中盤のカッティングギターがめちゃ良い～   

- USAO - Cthugha [Muse Dash]
[![](https://img.youtube.com/vi/_tX1lqcVnno/0.jpg)](https://www.youtube.com/watch?v=_tX1lqcVnno)

SpinelSCさんの選曲です。   
私、声ネタが好きなんですけどこの曲はそういう意味でこの曲はボーカル部分が好きですね～   
Spinelさんはいつも音ゲーの曲を流してくれてとても勉強になります～   
ゲーマー向け交流イベントを開かれているそうなのでゲーマーの方はぜひぜひ！   

- Henry Gallagher - Lightning Lyrics
[![](https://img.youtube.com/vi/f_2erk95-d0/0.jpg)](https://www.youtube.com/watch?v=f_2erk95-d0)

つもふかさんの選曲です。   
アコギの音が良いですね～   
歌詞が英語なので英語を聞き取れれば・・・　　　

- YOASOBI「祝福」
[![](https://img.youtube.com/vi/3eytpBOkOFA/0.jpg)](https://www.youtube.com/watch?v=3eytpBOkOFA)

つんつくつん！さんの選曲です。   
水星の魔女のOPだそうです。   
曲が素晴らしいのはもちろんですが、OPからして面白そうなアニメですね～・・・　　　
見てみようかな・・・

- Clair de lune
[![](https://img.youtube.com/vi/Vm8HeU4iaEI/0.jpg)](https://www.youtube.com/watch?v=Vm8HeU4iaEI)

Takahashiweltさんの選曲です。   
ドビュッシの月の光のアレンジだそうです。   
不思議な雰囲気の曲でとても好きですね～   

-  Ujico/SnailsHouse　ミスティック・ガール
[![](https://img.youtube.com/vi/yXru5LtpPoc/0.jpg)](https://www.youtube.com/watch?v=yXru5LtpPoc)

私の選曲です。   
これは冬に聴いていた曲で聴くと寒い冬を思い出しますね～   

- Queen Aluett -女王アルエット-
[![](https://img.youtube.com/vi/FNR6C03XKn8/0.jpg)](https://www.youtube.com/watch?v=FNR6C03XKn8)

SpinelSCさんの選曲です。   
ピアノが心地いい曲ですね～   
MVも面白くて良いです。   

- "魔王(World Ender)" sasakure.‌UK × TJ.hangneil
[![](https://img.youtube.com/vi/lAYw5ZTJztE/0.jpg)](https://www.youtube.com/watch?v=lAYw5ZTJztE)

kakoon/カクーンさんの選曲です。   
上手く言語化できないのですが面白い曲だと思いました・・・　　　
動画もとても良いです

- Amuro vs Killer - MEI (冥) [IIDXBGA WORKS]
[![](https://img.youtube.com/vi/EcF9k-v5Mng/0.jpg)](https://www.youtube.com/watch?v=EcF9k-v5Mng)

狐ノ巳 (konomi)さんの選曲です。   
途中でBPM上がるところが好きですね～   

- Squarepusher - Do You Know Squarepusher
[![](https://img.youtube.com/vi/rYS8edMRgBg/0.jpg)](https://www.youtube.com/watch?v=rYS8edMRgBg)

Takahashiweltさんの選曲です。   
めちゃ良い感じのテクノで好きです！　　　
こういうミニマルな曲とても好きです。   

- 美波「カワキヲアメク」
[![](https://img.youtube.com/vi/0YF8vecQWYs/0.jpg)](https://www.youtube.com/watch?v=0YF8vecQWYs)

Lunamia_さんの選曲です。　　　
歌詞が面白くて良いですね。   
歌い方も良いです。   

- Julian Muller - EURO 1
[![](https://img.youtube.com/vi/KDMLDJkLUx8/0.jpg)](https://www.youtube.com/watch?v=KDMLDJkLUx8)

私の選曲です。   
重いキックにボーカルが乗ってとても好きです。   
これくらいのスピードのDJがやりたくて最近いろいろ聴いてますね～　　　

## 感想・反省点   
今回は10名の方が来てくれました！   
今回はいつもと違い火曜日に開催したのですが多くの方に集まっていただけてとても嬉しいです！   
次回は金曜日に開催するのでよろしくお願いします～