---
date: 2023-2-17
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/57191394f402f8574792252af8b5e5368c7c0685/content/post/9/VRChat_2023-02-17_21-01-51.146_1920x1080.png"
tags: []
title: "第9回 Music Potluck Party"
---
第9回音楽投げ会を開催しました！

## 今回の参加者

- T4LLY
- Cipher-pCode-9
- かげひと
- SpinelSC
- アーネ・S
- gorexn
- UraHakuShu
- ices (あいす)

## 今回流れた音楽 

- Sari - 愛のゆりかご
[![](https://img.youtube.com/vi/na7l_AWyxOo/0.jpg)](https://www.youtube.com/watch?v=na7l_AWyxOo)

- Michael Wyckoff & Veronica Goudie - Would You Even Know 
[![](https://img.youtube.com/vi/kg5lzG0oCDQ/0.jpg)](https://www.youtube.com/watch?v=kg5lzG0oCDQ)

- Re:SONANCE
[![](https://img.youtube.com/vi/eUhvFAHrnKo/0.jpg)](https://www.youtube.com/watch?v=eUhvFAHrnKo)

- 月姫 -A piece of blue glass moon-
[![](https://img.youtube.com/vi/9B282GYZqCg/0.jpg)](https://www.youtube.com/watch?v=9B282GYZqCg)

- Alesso - Sweet Escape ft. Sirena
[![](https://img.youtube.com/vi/thpsmZiDv6U/0.jpg)](https://www.youtube.com/watch?v=thpsmZiDv6U)

- Eiffel 65 - Blue (Da Ba Dee) 1080p 60fps Full HD (High Quality)
[![](https://img.youtube.com/vi/NLg0C86_hZs/0.jpg)](https://www.youtube.com/watch?v=NLg0C86_hZs)

- JVKE - golden hour (official music video)
[![](https://img.youtube.com/vi/PEM0Vs8jf1w/0.jpg)](https://www.youtube.com/watch?v=PEM0Vs8jf1w)

- Guilty Gear Xrd REV 2 - Opening Movie
[![](https://img.youtube.com/vi/E0zqh9spQ7g/0.jpg)](https://www.youtube.com/watch?v=E0zqh9spQ7g)

- ARuFaオリジナルソング「さんさーら！」
[![](https://img.youtube.com/vi/Kl0ieRnAHnE/0.jpg)](https://www.youtube.com/watch?v=Kl0ieRnAHnE)

- Second Heaven BGA HD
[![](https://img.youtube.com/vi/wSotbjiqNU4/0.jpg)](https://www.youtube.com/watch?v=wSotbjiqNU4)

- EmoCosine - ネコジャラシ【from Muse Dash】
[![](https://img.youtube.com/vi/zH6sGTO99oI/0.jpg)](https://www.youtube.com/watch?v=zH6sGTO99oI)

- FreeTempo - Symmetry
[![](https://img.youtube.com/vi/TWCCJlf5dJk/0.jpg)](https://www.youtube.com/watch?v=TWCCJlf5dJk)

- 403 Northern Lights
[![](https://img.youtube.com/vi/MSR90cXfqKI/0.jpg)](https://www.youtube.com/watch?v=MSR90cXfqKI)

- IIDX EMPRESS - smooooch･∀･ (SPA) Autoplay [720p compatibility]
[![](https://img.youtube.com/vi/ysfpIkv95mU/0.jpg)](https://www.youtube.com/watch?v=ysfpIkv95mU)

- 【G2R2018】HyuN - Disorder【BGA】
[![](https://img.youtube.com/vi/6QGRs0H1tj0/0.jpg)](https://www.youtube.com/watch?v=6QGRs0H1tj0)

- Portal - Still Alive (1440p)
[![](https://img.youtube.com/vi/SXRteMSSZ14/0.jpg)](https://www.youtube.com/watch?v=SXRteMSSZ14)

## 感想・反省点   
今回は8名の方が来てくれました！   
記事が遅れてしまい申し訳ないです～!!!   
いつもありがとうございます！　　　