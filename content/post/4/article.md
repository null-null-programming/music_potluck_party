---
date: 2023-1-13
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/content/post/4/VRChat_2023-01-13_21-04-47.497_1920x1080.png"
tags: []
title: "第4回 Music Potluck Party"
---
第4回音楽投げ会を開催しました！

## 今回の参加者

- Goni2109さん
- iruma3802さん
- Takahashiweltさん
- ken_pikaさん
- zophie 83c7さん
- SpinelSCさん
- ふらっと (flat)さん
- xxseekxxさん
- tom_atomさん
- try_0514さん
- おにぎりの具さん
- Sinamoonさん

以上１２名の方が遊びに来てくれました！   
ありがとうございます～

## 今回流れた音楽 

- Cö shu Nie – 水槽のフール   
[![](https://img.youtube.com/vi/PRtySh3ZZnU/0.jpg)](https://www.youtube.com/watch?v=PRtySh3ZZnU)

iruma3802さんの選曲です。   
2:00からの展開が好きです。   
透明感のあるボーカルがとても良いです。   

- ぴ！/pi - ParaNoia D4nce
[![](https://img.youtube.com/vi/GHkv7Q2j49E/0.jpg)](https://www.youtube.com/watch?v=GHkv7Q2j49E)

私の選曲です。   
私がDJを始める前から聴いていた曲で疾走感のある展開がとても好きです。   

- a_hisa - Sand Maze
[![](https://img.youtube.com/vi/MrB9m35hLRM/0.jpg)](https://www.youtube.com/watch?v=MrB9m35hLRM)

SpinelSCさんの選曲です。   
アラビア風のメロディがとても良いです。   
音ゲーの曲だそうで、後半が難しそうです。   

- 吉澤嘉代子 - えらばれし子供たちの密話
[![](https://img.youtube.com/vi/FFIhw2Iz2QU/0.jpg)](https://www.youtube.com/watch?v=FFIhw2Iz2QU)

Takahashiweltさんの選曲です。   
ボーカルの歌い方がとても良いです。   

- KIEN - A Site De La Rue
[![](https://img.youtube.com/vi/30VGNHq6YHA/0.jpg)](https://www.youtube.com/watch?v=30VGNHq6YHA)

goni2109さんの選曲です。   
リズムの取り方が好きです。   

- サラのテーマ   
[![](https://img.youtube.com/vi/CRZQAJfVX3c/0.jpg)](https://www.youtube.com/watch?v=CRZQAJfVX3c)

ken_pikaさんの選曲です。   
クロノトリガーの曲だそうです。   
パーカッションがとても良いです。   

- Giga - 'G4L'
[![](https://img.youtube.com/vi/huO5w9GfSio/0.jpg)](https://www.youtube.com/watch?v=huO5w9GfSio)

irumaさんの選曲です。　　　
こういう曲はあまり聴かないのですが良いですね。   
声が好きです。   

- Cascade Masquerade
[![](https://img.youtube.com/vi/U_9f5Rx2FBI/0.jpg)](https://www.youtube.com/watch?v=U_9f5Rx2FBI)

tom_atomvrさんの選曲です。   
かなり好きです。   
動画もお洒落で良いですし、メロディがとても好みです。   

- somunia - Psykhe
[![](https://img.youtube.com/vi/ikmlUjNk1wM/0.jpg)](https://www.youtube.com/watch?v=ikmlUjNk1wM)

irumaさんの選曲です。   
不思議な雰囲気の曲でボーカルの歌い方がとても良いです。   

- 和ぬか - 絶頂讃歌
[![](https://img.youtube.com/vi/pLBX9vdrtn4/0.jpg)](https://www.youtube.com/watch?v=pLBX9vdrtn4)

ふらっと (flat)さんの選曲です。   
ギターソロがかっこよくて好きです。   

- Shutoku Mukai and Sheena Ringo - KIMOCHI
[![](https://img.youtube.com/vi/WMQZcVgjk8k/0.jpg)](https://www.youtube.com/watch?v=WMQZcVgjk8k)

xxseekxxさんの選曲です。   
歌とギターがとても良いです。   

- USAO - Big Daddy
[![](https://img.youtube.com/vi/W6a1C1ojzH0/0.jpg)](https://www.youtube.com/watch?v=W6a1C1ojzH0)

SpinelSCさんの選曲です。   
重い音が良いですね。   
音ゲーの曲だそうです。   

- Yellow Magic Orchestra - Technopolis (1979)
[![](https://img.youtube.com/vi/zFADf3CW8zI/0.jpg)](https://www.youtube.com/watch?v=zFADf3CW8zI)

ken_pikaさんの選曲です。   
YMOは聴いたことが無かったのですが、とても良いですね。   
聴く機会が無かったのでこの会を開いて良かったと思いました。   

- Dr. Feelgood - Roxette
[![](https://img.youtube.com/vi/Yp2DvPKh118/0.jpg)](https://www.youtube.com/watch?v=Yp2DvPKh118)

Takahashiweltさんの選曲です。   
ハーモニカがかっこいいですね～　　　
レトロな雰囲気がとても良いです。   

- kamomesano - waterproof
[![](https://img.youtube.com/vi/6YnUkXKQ0Qk/0.jpg)](https://www.youtube.com/watch?v=6YnUkXKQ0Qk)

私の選曲です。   
かなしい気持ちが続いた時期に聴いていた曲なので、この曲を聴くとかなしくなります。   
曲自体はGarage感のあるノリの良い曲でとても好きです。   


## 感想・反省点   
今回は１２名の方が来てくれました！   
前回に引き続き沢山の人が来てくれてめちゃ嬉しいです～   
みなさまいつもありがとうございます！