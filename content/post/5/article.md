---
date: 2023-1-20
featured_image: "https://gitlab.com/null-null-programming/music_potluck_party/-/raw/master/content/post/7/VRChat_2023-02-03_21-01-47.162_1920x1080.png"
tags: []
title: "第5回 Music Potluck Party"
---
第5回音楽投げ会を開催しました！

## 今回の参加者

- Kyaru3333さん
- bimoyuさん
- ken_pikaさん
- 狐ノ巳 (konomi)さん
- ふらっと (flat)さん
- JAE Youngさん
- SpinelSCさん
- KuRo_IrOさん
- あたごまるさん

以上9名の方が遊びに来てくれました！   
ありがとうございます～

## 今回流れた音楽 

- 「デレステ」廻談詣り (Game ver.) 依田芳乃、白坂小梅 SSR
[![](https://img.youtube.com/vi/EEyShyhReNk/0.jpg)](https://www.youtube.com/watch?v=EEyShyhReNk)

bimoyuさんの選曲です。   
歌が可愛いです。   
動画もストーリーがあって面白いですね～   

- Shunji Fujii - Colors
[![](https://img.youtube.com/vi/WaMqQkuim20/0.jpg)](https://www.youtube.com/watch?v=WaMqQkuim20)

私の選曲です。   
日本の作曲家Shunji Fujiiさんの曲です。   
特徴的な作風でとても好きで、聴いたことがなくても一発でわかりそうです。   

- Asian Kung-Fu Generation - 振動覚
[![](https://img.youtube.com/vi/xokQeXsnrT4/0.jpg)](https://www.youtube.com/watch?v=xokQeXsnrT4)

ken_pikaさんの選曲です。   
特徴的なボーカルがとても良いです。   
ロックも良いですね～   

- キヤロラ衛星の軌跡 ／ 工藤吉三
[![](https://img.youtube.com/vi/dbghHvvajaM/0.jpg)](https://www.youtube.com/watch?v=dbghHvvajaM)

狐ノ巳 (konomi)さんの選曲です。   
音ゲーの曲だそうです。   
ギターとベースがとてもかっこいいですね　　　

- snow jam - Rin音 (Official Music Video)
[![](https://img.youtube.com/vi/qucqaX8AHeg/0.jpg)](https://www.youtube.com/watch?v=qucqaX8AHeg)

ふらっと　(flat)さんの選曲です。
ボーカルが良いですね。   

- Tic! Tac! Toe! by TAK x Corbin
[![](https://img.youtube.com/vi/h5hMNF3kDm0/0.jpg)](https://www.youtube.com/watch?v=h5hMNF3kDm0)

bimoyuさんの選曲です。   
開幕の音が良すぎて耳が幸せでした。   
韓国語の歌いいですよね～　　　

ー 彼の者の名は･･･
[![](https://img.youtube.com/vi/xZ9LK6vsWbY/0.jpg)](https://www.youtube.com/watch?v=xZ9LK6vsWbY)

Kyaru3333さんの選曲です。   
情熱的な曲調でとても良いです。    

- NB RANGERS - 운명의 Destiny by NieN
[![](https://img.youtube.com/vi/zciUQ98PTCU/0.jpg)](https://www.youtube.com/watch?v=zciUQ98PTCU)

SpinelSCさんの選曲です。   
動画が面白かったです。   

- Leahy - B Minor
[![](https://img.youtube.com/vi/QsIcTsqtOaE/0.jpg)](https://www.youtube.com/watch?v=QsIcTsqtOaE)

狐ノ巳 (konomi)さんの選曲です。　　　
ヴァイオリンいいですね～   
ノリの良い音がとても好きです。   

- the sub account - 文月
[![](https://img.youtube.com/vi/aIg7R7zeJzE/0.jpg)](https://www.youtube.com/watch?v=aIg7R7zeJzE)

私の選曲です。　　　
the sub accountさんの曲好きなんですよね～   
韓国語の独特な感じがとても良いです。   

- Janne Da Arc - 月光花
[![](https://img.youtube.com/vi/tHk4thfcL9Q/0.jpg)](https://www.youtube.com/watch?v=tHk4thfcL9Q)

Kyaru3333さんの選曲です。   
ボーカルが良いですね。   
ギターソロもとても良いです。   

- DJMAX - 風にお願い(JPver)
[![](https://img.youtube.com/vi/qY6evHYZoWk/0.jpg)](https://www.youtube.com/watch?v=qY6evHYZoWk)

KuRo_IrOさんの選曲です。
これもDJMAXの曲ですね。   
JP.verということはほかのバージョンもあるのでしょうか？   

- 【重音テト】紫煙
[![](https://img.youtube.com/vi/NOnmXBtGMkA/0.jpg)](https://www.youtube.com/watch?v=NOnmXBtGMkA)

ふらっと (flat)さんの選曲です。   
紫煙ということでタバコを匂わせる曲です。   
ほかの人がタバコを吸っていると自分も吸いたくなるのですが、喫煙者の方々はどうでしょうか？   

- Dream of Winds - XeoN
[![](https://img.youtube.com/vi/iha5GCtyyMk/0.jpg)](https://www.youtube.com/watch?v=iha5GCtyyMk)

SpinelSCさんの選曲です。   
ピアノが綺麗な曲です。   

- ペニシリン/Romance - PENICILLIN「ロマンス」
[![](https://img.youtube.com/vi/18H2Uc9_7I0/0.jpg)](https://www.youtube.com/watch?v=18H2Uc9_7I0)

Kyaru3333さんの選曲です。   
特徴的なボーカルが良いですね   

- DJ Myosuke & Gram - XODUS [Muse Dash]
[![](https://img.youtube.com/vi/qubAtSaa_U8/0.jpg)](https://www.youtube.com/watch?v=qubAtSaa_U8)

SpinelSCさんの選曲です。   
だいぶ激しいなと思ったらメロディックな雰囲気になって静かになってと展開が沢山ある曲でした。   

- Daiki Kasho - Day To Live | GT Sport MV
[![](https://img.youtube.com/vi/5u-rY3KTtZk/0.jpg)](https://www.youtube.com/watch?v=5u-rY3KTtZk)

狐ノ巳 (konomi)産の選曲です。   
グランツーリスモの曲だそうです。   
スポーティーな曲でとても好みです。   

- 宇多田ヒカル『 One Last Kiss』
[![](https://img.youtube.com/vi/0Uhh62MUEic/0.jpg)](https://www.youtube.com/watch?v=0Uhh62MUEic)

JAE Youngさんの選曲です。   
曲はもちろん良いのですが、MVの動画技術にしびれました。   
エンディング感があって最後の曲としてとても良いですね。

## 感想・反省点   
今回は9名の方が来てくれました！   
このイベントを始めて１カ月が経ちましたが、いろいろな人が来てくれて本当に楽しいです。   
気長に続けていくのでお気軽にお越しください～
次回は火曜日です！